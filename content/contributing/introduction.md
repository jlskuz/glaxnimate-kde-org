---
title: Contributing To Glaxnimate
layout: get-involved
hideMeta: true
author: Mattia Basaglia
menu:
  main:
    name: Introduction
    parent: contributing
    weight: 1
no_text: true
---

You can make the difference!

## Donating

If you don't have any technical skill you can still support Glaxnimate by donating.

## Reporting issues, bugs and feature requests

You can report issues on [GitLab](https://gitlab.com/mattbas/glaxnimate/-/issues). Bug reports and feature requests are welcome.

When reporting bugs please be as detailed as possible, include steps to reproduce the issue and describe what happens. If relevant, attach the file you were editing on the issue.

You can also add the system information gathered by Glaxnimate itself: Go to _Help > About... > System_ and click _Copy_, then you can paste on the issue the system information.

## Code

See the [README](https://invent.kde.org/graphics/glaxnimate/-/blob/master/README.md) for build instructions.

You can open [merge requests on GitLab](https://invent.kde.org/graphics/glaxnimate/-/merge_requests) to get your changes merged into Glaxnimate.

## Packaging

You can create packages to port Glaxnimate to a specific system, setting up an automatable process to create said package so it can be integrated with continuous integration.

Existing Packages can be found on the [download page](/download). If you want to port Glaxnimate to a different system of package manager, feel free to do so!

<img src="/reusable-assets/konqi-docbook.png" alt="" class="konqi">

## Documentation

You can add to the documentation by adding tutorials, missing information, correcting typos, etc...

On the [Documentation Website](https://docs.glaxnimate.org) each page has a link to its source file on GitLab, you can use that page to edit it and create a pull request.

Details on how to work with the documentation are at [Documentation](/contributing/documentation/).

## Translations

You can add or improve translations. See [Localization](https://community.kde.org/Get_Involved/translation) for more details.


### License

Glaxnimate is licensed under the [GNU GPLv3+](http://www.gnu.org/licenses/gpl-3.0.html), for documentation contributions the license is dual GPLv3+ and CC BY-SA 4.0.

For everything else in the Glaxnimate repository, the license is GPLv3+. Some submodules have their own licensing terms.

If you contribute to the Glaxnimate, you agree that your contributions are compatible with the licensing terms.


## Wait, there is more...

There are plenty of other tasks that you can help us with to make Glaxnimate better even if you don't know any programming languages!

- [Bug triaging](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging) - help us find mis-filed, duplicated or invalid bug reports in Bugzilla
- [Documentation](https://community.kde.org/Get_Involved/documentation) - help us improve user documentation to make more friendly for newcomers
- [Promotion](https://community.kde.org/Get_Involved/promotion) - help us promote both online and offline

Do you have any other idea? Get in touch!

